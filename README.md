This package is intended to support the UTRC03 Husky.  It is equipped with a Velodyne LIDAR, 
four Point Grey FireflyMV cameras, dual Novatel RTK-capable GPS antennas, and a SICK LMS111
LIDAR mounted on a FLIR pan-tilt unit.

The GPS, Velodyne and cameras are time-synchronized by a beta version hardware synchronization
box.  
